package org.bryampaniagua.utilidades;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ProgressBar;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class App extends Application{
	
	private Scene primaryScene;
	private GridPane gpCont1, gpCont2;
	private BorderPane bpCont1;
	private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btnPanel;
	private Label lbEtiqueta1, lbEtPanel;
	private TextField tfTexto;
	private ProgressBar pbBarra1;
	
	public void start(Stage primaryStage) throws Exception{
		
		primaryStage.setTitle("Panel");
		
		btn1 = new Button("  Boton  ");
		GridPane.setMargin(btn1, new Insets(3,3,3,3));
		
		btn2 = new Button("  Boton  ");
		GridPane.setMargin(btn2, new Insets(3,3,3,3));
		
		btn3 = new Button("  Boton  ");
		GridPane.setMargin(btn3, new Insets(3,3,3,3));
		
		btn4 = new Button("  Boton  ");
		GridPane.setMargin(btn4, new Insets(3,3,3,3));
		
		btn5 = new Button("  Boton  ");
		GridPane.setMargin(btn5, new Insets(3,3,3,3));
		
		btn6 = new Button("  Boton  ");
		GridPane.setMargin(btn6, new Insets(3,3,3,3));
		
		btn7 = new Button("  Boton  ");
		GridPane.setMargin(btn7, new Insets(3,3,3,3));
		
		btnPanel = new Button("BotonPanel");
		GridPane.setMargin(btnPanel, new Insets(3,3,3,3));
		
		lbEtiqueta1 = new Label("Label");
		GridPane.setMargin(lbEtiqueta1, new Insets(3,3,3,3));
		
		tfTexto = new TextField();
		GridPane.setMargin(tfTexto, new Insets(3,3,3,3));
		tfTexto.setPrefWidth(50);
		
		pbBarra1 = new ProgressBar();
		GridPane.setMargin(pbBarra1, new Insets(3,3,3,3));
		pbBarra1.setPrefWidth(300);
		
		lbEtPanel = new Label("PANEL");
		BorderPane.setMargin(lbEtPanel, new Insets(3,3,3,3));
		
		bpCont1 = new BorderPane();
		BorderPane.setMargin(bpCont1, new Insets(3,3,3,3));
		bpCont1.setStyle("-fx-background-color: #AACCFF;");	
		bpCont1.setCenter(lbEtPanel);
		
		gpCont1 = new GridPane();
		// gpCont1.setGridLinesVisible(true);
		gpCont1.setStyle("-fx-background-color: #80BFFF;");
		gpCont1.setAlignment(Pos.CENTER);
		
		gpCont1.add(btn1 , 0,0);
		gpCont1.add(btn2 , 0,1);
		gpCont1.add(btn3 , 0,2);
		gpCont1.add(btn4 , 1,3);
		gpCont1.add(btn5 , 2,4);
		gpCont1.add(btn6 , 3,5);
		gpCont1.add(btn7 , 3,6);
		gpCont1.add(btnPanel , 0,6);
		gpCont1.add(lbEtiqueta1 , 3,0 , 2,5);
		gpCont1.add(bpCont1 , 1,0 , 2,3);
		gpCont1.add(tfTexto, 1,5 , 2,1);
		gpCont1.add(pbBarra1, 0,7 , 5,1);
		
		primaryScene = new Scene(gpCont1);
		
		primaryStage.setScene(primaryScene);
		primaryStage.show();
		
	}

}