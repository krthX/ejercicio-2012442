package org.bryampaniagua.sistema;

import javafx.application.Application;

import org.bryampaniagua.utilidades.App;

public class Principal{
	public static void main(String[] args){
		Application.launch(App.class, args);
	}
}